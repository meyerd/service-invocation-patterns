package com.camunda.fox.example;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.activiti.cdi.BusinessProcess;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.domain.Rating;

@RunWith(Arquillian.class)
public class TestCreditRatingProcessSync extends AbstractFoxPlatformIntegrationTest{
  
  @Deployment
  public static WebArchive createDeployment() {
    return initWebArchiveDeployment()
            .addPackages(true, "com.camunda.fox.example")
            .addAsResource("creditRatingSyncContinuation.bpmn20.xml")           
            .addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml");
  }
  
  @Inject 
  private BusinessProcess businessProcess;
      
  @PersistenceContext 
  private EntityManager entityManager;
  
  @Inject
  private UserTransaction utx;
  
  @Test
  public void testPositiveScore() {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Daniel");    
    loanApplication.setLastname("Meyer");    
    businessProcess.setVariable("loanApplication", loanApplication);    
    
    ProcessInstance pi = businessProcess.startProcessByKey("creditRatingSync");

    Assert.assertNotNull(historyService.createHistoricActivityInstanceQuery()
      .processInstanceId(pi.getId())
      .activityId("generateContract")
      .singleResult());
    
    Rating rating = findRating();
    Assert.assertTrue(rating.getScore()>0);
        
  }
 
  @Test
  public void testNegativeScore() {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Bernd");    
    loanApplication.setLastname("Ruecker");    
    businessProcess.setVariable("loanApplication", loanApplication);    
    
    ProcessInstance pi = businessProcess.startProcessByKey("creditRatingSync");
    
    Assert.assertNotNull(historyService.createHistoricActivityInstanceQuery()
            .processInstanceId(pi.getId())
            .activityId("generateRejectionLetter")
            .singleResult());
    
    Rating rating = findRating();
    Assert.assertTrue(rating.getScore()<0);
    
  }
    
  @After
  public void cleanup() throws Exception {
    utx.begin();
    try {
      Rating raiting = findRating();     
      entityManager.remove(raiting);     
      utx.commit();
    }catch (Exception e) {
      utx.rollback();
    }
  }
  
  @Test
  public void testFailureCase() {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Jakob");    
    loanApplication.setLastname("Freund");    
    businessProcess.setVariable("loanApplication", loanApplication);    
    
    try {
      // since the service task is invoked synchronously, the exception is propagated back to us. 
      ProcessInstance pi = businessProcess.startProcessByKey("creditRatingSync");
      Assert.fail("this should have failed!");
    }catch (ActivitiException e) {
      
    }
    
    try {    
      findRating();
      Assert.fail("this should have failed!");
    }catch (NoResultException e) {
      // there is no raiting
    } 
    
  }
  
  private Rating findRating() {
    return (Rating) entityManager.createQuery("select r from Rating r")
            .getSingleResult();
  }

}
