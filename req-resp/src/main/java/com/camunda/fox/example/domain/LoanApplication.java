package com.camunda.fox.example.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import org.activiti.cdi.annotation.BusinessProcessScoped;

@BusinessProcessScoped
public class LoanApplication implements Serializable {

  private static final long serialVersionUID = 1L;
  
  private String firstname;
  private String lastname;
  private BigDecimal loanAmmount;

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public BigDecimal getLoanAmmount() {
    return loanAmmount;
  }

  public void setLoanAmmount(BigDecimal loanAmmount) {
    this.loanAmmount = loanAmmount;
  }

  public Calendar getLoanStartDate() {
    return loanStartDate;
  }

  public void setLoanStartDate(Calendar loanStartDate) {
    this.loanStartDate = loanStartDate;
  }

  public Calendar getLoanEndDate() {
    return loanEndDate;
  }

  public void setLoanEndDate(Calendar loanEndDate) {
    this.loanEndDate = loanEndDate;
  }

  private Calendar loanStartDate;
  private Calendar loanEndDate;

}
