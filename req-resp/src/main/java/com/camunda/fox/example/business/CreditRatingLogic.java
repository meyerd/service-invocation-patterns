package com.camunda.fox.example.business;

import com.camunda.fox.example.domain.LoanApplication;


public class CreditRatingLogic {

  public static int rateApplication(LoanApplication loanApplication) {
        
    if("Bernd".equals(loanApplication.getFirstname()) 
            && "Ruecker".equals(loanApplication.getLastname())) {
      
      // Bernd does not receive any more loans
      return -100;     
      
    } else if("Jakob".equals(loanApplication.getFirstname()) 
              && "Freund".equals(loanApplication.getLastname())) {
      
      throw new RuntimeException("I don't know what to do about Jakob!");
      
    } else {
      
      // everyone else receives a positive raiting
      return 100;
      
    }
    
  }
  
}
