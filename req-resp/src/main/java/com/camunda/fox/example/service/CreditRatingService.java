package com.camunda.fox.example.service;

import java.util.Map;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.camunda.fox.example.business.CreditRatingLogic;
import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.domain.Rating;

@Named
public class CreditRatingService {
    
  @Inject @Named
  private Map<String, Object> processVariables;
  
  @PersistenceContext
  private EntityManager entityManager;
  
  
  public void rateApplication() {
    
    LoanApplication loanApplication = (LoanApplication) processVariables.get("loanApplication");
    
    int score = CreditRatingLogic.rateApplication(loanApplication);
    
    Rating rating = new Rating();    
    rating.setScore(score);
    
    entityManager.persist(rating);
    entityManager.flush();
    
    processVariables.put("ratingId", rating.getId());
  }
  
  @Produces @Named
  public Rating rating() {
    return entityManager.find(Rating.class, processVariables.get("ratingId"));
  }

}
