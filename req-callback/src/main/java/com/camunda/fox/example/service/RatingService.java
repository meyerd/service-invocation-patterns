package com.camunda.fox.example.service;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.activiti.cdi.BusinessProcess;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;

import com.camunda.fox.example.business.CreditRatingLogic;
import com.camunda.fox.example.domain.Rating;
import com.camunda.fox.example.evt.LoanApplicationEvent;
import com.camunda.fox.example.evt.ReadyToRate;

@Stateless
public class RatingService {

  @PersistenceContext
  private EntityManager entityManager;
  
  @Inject 
  private CreditRatingLogic creditRatingLogic;
  
  @Inject 
  private RuntimeService runtimeService;
  
  @Inject
  private BusinessProcess businessProcess;
    
  @Asynchronous
  public void rateApplication(
          @Observes(during=TransactionPhase.AFTER_SUCCESS) 
          @ReadyToRate 
          LoanApplicationEvent loanApplicationEvent) {
        
    int score = creditRatingLogic.rateApplication(loanApplicationEvent.getLoanApplication());
    
    Rating rating = new Rating();    
    rating.setScore(score);
    
    entityManager.persist(rating);
    entityManager.flush();
    
    Execution execution = runtimeService.createExecutionQuery()
      .processInstanceId(loanApplicationEvent.getCorrelationId())
      .activityId("receiveRating")
      .singleResult();
        
    businessProcess.associateExecutionById(execution.getId());    
    businessProcess.setVariable("ratingId", rating.getId());
    businessProcess.signalExecution();    
  }
  
}
