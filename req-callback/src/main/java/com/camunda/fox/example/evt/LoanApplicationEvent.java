package com.camunda.fox.example.evt;

import com.camunda.fox.example.domain.LoanApplication;

public class LoanApplicationEvent {

  private final LoanApplication loanApplication;
  private final String correlationId;

  public LoanApplicationEvent(LoanApplication loanApplication, String correlationId) {
    this.loanApplication = loanApplication;
    this.correlationId = correlationId;
  }

  public LoanApplication getLoanApplication() {
    return loanApplication;
  }

  public String getCorrelationId() {
    return correlationId;
  }

}
