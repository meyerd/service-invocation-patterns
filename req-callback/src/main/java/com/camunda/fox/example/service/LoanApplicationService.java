package com.camunda.fox.example.service;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.cdi.BusinessProcess;

import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.evt.LoanApplicationEvent;
import com.camunda.fox.example.evt.ReadyToRate;

@Named
public class LoanApplicationService {

  @Inject
  @ReadyToRate
  private Event<LoanApplicationEvent> evt;
  
  @Inject
  private BusinessProcess businessProcess;

  public void newApplication(LoanApplication loanApplication) {
    
    String executionId = businessProcess.getExecution().getId(); 
    evt.fire(new LoanApplicationEvent(loanApplication, executionId));
  }

}
