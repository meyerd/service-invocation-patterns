package com.camunda.fox.example.business;

import javax.enterprise.context.ApplicationScoped;

import com.camunda.fox.example.domain.LoanApplication;

@ApplicationScoped
public class CreditRatingLogic {

  public int rateApplication(LoanApplication loanApplication) {
    
    if("Bernd".equals(loanApplication.getFirstname()) 
            && "Ruecker".equals(loanApplication.getLastname())) {
      
      // Bernd does not receive any more loans
      return -100;     
      
    } else if("Jakob".equals(loanApplication.getFirstname()) 
              && "Freund".equals(loanApplication.getLastname())) {
      
      throw new RuntimeException("I don't know what to do about Jakob!");
      
    } else {
      
      // everyone else receives a positive rating
      return 100;
      
    }
    
  }
  
}
