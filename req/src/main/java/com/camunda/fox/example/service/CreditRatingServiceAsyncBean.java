package com.camunda.fox.example.service;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.camunda.fox.example.business.CreditRatingLogic;
import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.domain.Rating;

@Stateless
@Named("creditRatingService")
public class CreditRatingServiceAsyncBean {
      
  @PersistenceContext
  private EntityManager entityManager;
  
  @Inject
  private CreditRatingLogic creditRatingLogic;
  
  // Invoked by the EJB Container in a separate background thread
  @Asynchronous  
  public void rateApplication(LoanApplication loanApplication) {
    
    int score = creditRatingLogic.rateApplication(loanApplication);
    
    Rating raiting = new Rating();    
    raiting.setScore(score);
    
    entityManager.persist(raiting);
    entityManager.flush();    
  }
  
}
