package com.camunda.fox.example;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.activiti.cdi.BusinessProcess;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.domain.Rating;

@RunWith(Arquillian.class)
public class TestCreditRatingProcessJms extends AbstractFoxPlatformIntegrationTest{
  
  @Deployment
  public static WebArchive createDeployment() {
    return initWebArchiveDeployment()
            .addPackages(true, "com.camunda.fox.example")
            .addAsResource("creditRatingJms.bpmn20.xml")           
            .addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml");
  }
  
  @Inject 
  private BusinessProcess businessProcess;
      
  @PersistenceContext 
  private EntityManager entityManager;
  
  @Inject
  private UserTransaction utx;
  
  @Test
  public void testPositiveScore() throws InterruptedException {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Daniel");    
    loanApplication.setLastname("Meyer");    
    businessProcess.setVariable("loanApplication", loanApplication);    
    
    businessProcess.startProcessByKey("creditRating");
    
    Thread.sleep(2000);
        
    Rating rating = findRating();
    Assert.assertTrue(rating.getScore()>0); // positive rating
        
  }
 
  @Test
  public void testNegativeScore() throws InterruptedException {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Bernd");    
    loanApplication.setLastname("Ruecker");    
    businessProcess.setVariable("loanApplication", loanApplication);    
    
    businessProcess.startProcessByKey("creditRating");
    
    Thread.sleep(2000);
    
    Rating rating = findRating();
    Assert.assertTrue(rating.getScore()<0); // negative rating
    
  }
  
  @Test
  public void testFailureCase() throws InterruptedException {
    
    LoanApplication loanApplication = new LoanApplication();
    loanApplication.setFirstname("Jakob");    
    loanApplication.setLastname("Freund");    
    businessProcess.setVariable("loanApplication", loanApplication);    

    // since the service itself task is invoked asynchronously, 
    // the exception is not propagated back to us.
    businessProcess.startProcessByKey("creditRating");
    
    // wait while the messaging system attempts to deliver the message
    Thread.sleep(2000);
    
    try {    
      findRating();
      Assert.fail("this should have failed!");
    }catch (NoResultException e) {
      // the rating is not created, but the message is still in the jms queue 
      // or in a dead letter queue
    } 
    
  }

  @After
  public void cleanup() throws Exception {
    utx.begin();
    try {
      Rating rating = findRating();
      entityManager.remove(rating);
      utx.commit();
    } catch (Exception e) {
      utx.rollback();
    }
  }

  private Rating findRating() {
    return (Rating) entityManager.createQuery("select r from Rating r").getSingleResult();
  }

}
