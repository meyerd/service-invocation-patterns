package com.camunda.fox.example.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
public class Rating  {

  @Id
  @GeneratedValue
  private Long id;

  @Version
  private Long version;

  private int score;

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }
    
  public Long getId() {
    return id;
  }

}
