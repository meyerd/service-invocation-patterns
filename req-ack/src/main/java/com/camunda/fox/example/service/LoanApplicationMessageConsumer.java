package com.camunda.fox.example.service;

import java.util.concurrent.atomic.AtomicInteger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.camunda.fox.example.business.CreditRatingLogic;
import com.camunda.fox.example.domain.LoanApplication;
import com.camunda.fox.example.domain.Rating;

@MessageDriven(activationConfig = {
  @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
  @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/queue/test"),
  @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") }
) 
public class LoanApplicationMessageConsumer implements MessageListener {
  
  static AtomicInteger attempts = new AtomicInteger();

  @PersistenceContext
  private EntityManager entityManager;
  
  @Inject
  private CreditRatingLogic creditRatingLogic;
  
  @Override
  public void onMessage(Message message) { 
    
    if (message instanceof ObjectMessage) {
      ObjectMessage objectMessage = (ObjectMessage) message;
      try {
        
        // NOTE: this is for testing only
        if(attempts.compareAndSet(3, 4)) {
          // allow the 3rd attempt to succeed
          return; 
        } else if(message.getJMSRedelivered()) {
          attempts.incrementAndGet();    
        } else {
          attempts.set(0);
        }
                
        LoanApplication application = (LoanApplication) objectMessage.getObject();
        
        int score = creditRatingLogic.rateApplication(application);
        
        Rating r = new Rating();
        r.setScore(score);
        
        entityManager.persist(r);
        
      } catch (JMSException e) {
        throw new EJBException("Could not unwrap object message" + e.getMessage(), e);
      }      
    } else {
      throw new EJBException("Message must be of type ObjectMessage");
    }
    
    
  }
}
