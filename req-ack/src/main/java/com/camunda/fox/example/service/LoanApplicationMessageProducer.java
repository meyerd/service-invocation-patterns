package com.camunda.fox.example.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import com.camunda.fox.example.domain.LoanApplication;

@Named
@Stateless
public class LoanApplicationMessageProducer {

  private final static Logger log = Logger.getLogger(LoanApplicationMessageProducer.class.getName());

  @Resource(name = "java:/queue/test")
  private Queue queue;
  @Resource(name = "java:/ConnectionFactory")
  private ConnectionFactory connectionFactory;

  public void rateApplication(LoanApplication loanApplication) {
    Connection connection = null;
    Session session = null;
    try {
      connection = connectionFactory.createConnection();
      session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer messageProducer = session.createProducer(queue);

      ObjectMessage loanApplicationMessage = session.createObjectMessage(loanApplication);
      messageProducer.send(loanApplicationMessage);      

    } catch (Exception e) {
      throw new EJBException("Exception while sending message: " + e.getMessage(), e);
    } finally {
      if (session != null) {
        try {
          session.close();
        } catch (JMSException e) {
          log.log(Level.SEVERE, "Cannot close session: " + e.getMessage(), e);
        }
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (JMSException e) {
          log.log(Level.SEVERE, "Cannot close connection: " + e.getMessage(), e);
        }
      }
    }
  }

}
